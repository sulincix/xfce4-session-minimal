build:
	: please run make install
install:
	mkdir -p ${DESTDIR}/usr/bin || true
	mkdir -p ${DESTDIR}/usr/share/xsessions || true
	install startxfce4-minimal ${DESTDIR}/usr/bin
	install xfce-minimal.desktop ${DESTDIR}/usr/share/xsessions
